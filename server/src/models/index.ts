
import mongoose from 'mongoose';
import {
  IEntryObjectModel, GeneralSeat, GeneralPwDSeat, EWSSeat, EWSPwDSeat,
  OBCNCLSeat, OBCNCLPwDSeat, SCSeat, SCPwDSeat,
  STSeat, STPwDSeat
} from './entryObject';

const connectDb = () => {
  const url = (process.env.TEST_DATABASE_URL || process.env.DATABASE_URL) as string;
  return mongoose.connect(url, { useNewUrlParser: true });
};

export type Models = IEntryObjectModel;

export type ModelsMap = { [key: string]: Models };

const models: ModelsMap = {
  GeneralSeat, GeneralPwDSeat, EWSSeat, EWSPwDSeat,
  OBCNCLSeat, OBCNCLPwDSeat, SCSeat, SCPwDSeat,
  STSeat, STPwDSeat
};

export { connectDb };

export default models;