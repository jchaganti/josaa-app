import mongoose, { Schema, Model, Document } from 'mongoose';


const EntryObjectSchema = new Schema({
  institute: {
    type: String,
    required: true,
    index: {background: false}
  },
  type: {
    type: Number,
    required: true,
    index: {background: false}
  },
  program: {
    type: String,
    required: true,
    index: {background: false}
  },
  quota: {
    type: String,
    required: true,
    index: {background: false}
  },
  seatType: {
    type: String,
    required: true
  },
  gender: {
    type: String,
    required: true,
    index: {background: false}
  },
  or: {
    type: Number,
    required: true
  },
  cr: {
    type: Number,
    required: true
  },
  year: {
    type: Number,
    required: true,
    index: {background: false}
  },
  round: {
    type: Number,
    required: true,
    index: {background: false}
  }
});

// Instance methods
export interface IEntryObject extends Document {
}

// Static methods
export interface IEntryObjectModel extends Model<IEntryObject> {
}

export const GeneralSeat: IEntryObjectModel = mongoose.model<IEntryObject, IEntryObjectModel>('GeneralSeat', EntryObjectSchema);
export const GeneralPwDSeat: IEntryObjectModel = mongoose.model<IEntryObject, IEntryObjectModel>('GeneralPwDSeat', EntryObjectSchema);
export const EWSSeat: IEntryObjectModel = mongoose.model<IEntryObject, IEntryObjectModel>('EWSSeat', EntryObjectSchema);
export const EWSPwDSeat: IEntryObjectModel = mongoose.model<IEntryObject, IEntryObjectModel>('EWSPwDSeat', EntryObjectSchema);
export const OBCNCLSeat: IEntryObjectModel = mongoose.model<IEntryObject, IEntryObjectModel>('OBCNCLSeat', EntryObjectSchema);
export const OBCNCLPwDSeat: IEntryObjectModel = mongoose.model<IEntryObject, IEntryObjectModel>('OBCNCLPwDSeat', EntryObjectSchema);
export const SCSeat: IEntryObjectModel = mongoose.model<IEntryObject, IEntryObjectModel>('SCSeat', EntryObjectSchema);
export const SCPwDSeat: IEntryObjectModel = mongoose.model<IEntryObject, IEntryObjectModel>('SCPwDSeat', EntryObjectSchema);
export const STSeat: IEntryObjectModel = mongoose.model<IEntryObject, IEntryObjectModel>('STSeat', EntryObjectSchema);
export const STPwDSeat: IEntryObjectModel = mongoose.model<IEntryObject, IEntryObjectModel>('STPwDSeat', EntryObjectSchema);

const models = [GeneralSeat, GeneralPwDSeat, EWSSeat, EWSPwDSeat, OBCNCLSeat, OBCNCLPwDSeat, SCSeat, SCPwDSeat, STSeat, STPwDSeat];

models.forEach((model: IEntryObjectModel) => {
  model.on('index', function(err, i) {
    if (err) {
        console.error('model index error: %s', err);
    } else {
        console.info('model indexing complete');
    }
});
})