// import { ApolloServer, AuthenticationError } from 'apollo-server-express';
// import cors from 'cors';
// import DataLoader from 'dataloader';
// import express from 'express';
// import http from 'http';
// import jwt, { Secret } from 'jsonwebtoken';
// import morgan from 'morgan';
// import passport from 'passport';
// import googlePassport from 'passport-google-oauth20';
// import fbPassport from 'passport-facebook';
// import loaders from './loaders';
// import models, { connectDb } from './models';
// import resolvers from './resolvers';
// import schema from './schema';

// import 'dotenv/config';
// import { IUserModel, IUser } from './models/user';
// import { cast } from './utils';
// import cookieParser from 'cookie-parser';
// import mustacheExpress from 'mustache-express';
// import sizeof from 'object-sizeof'
// const GoogleStrategy = googlePassport.Strategy;
// const FacebookStrategy = fbPassport.Strategy;

// const handleProfileCreation = async (_accessToken: any, _refreshToken: any, profile: any, done: Function) => {
//   const UserModel: IUserModel = cast(models.User);
//   const user: IUser = await UserModel.findByProviderAndId(profile.provider, profile.id);
//   if (user) {
//     return done(undefined, user);
//   } else {
//     const emails = profile.emails?.map((email: any) => email.value);
//     const _user: IUser = new UserModel({
//       provider: profile.provider,
//       pid: profile.id,
//       displayName: profile.displayName,
//       emails: emails
//     });
//     try {
//       const user = await _user.save();
//       console.log('@@@ user created', sizeof(user) )
//       return done(undefined, user);
//     } catch (e) {
//       console.log('Error during Google user creation for profile', e, profile);
//       return done(undefined, e);
//     }
//   }
// }
// passport.use(
//   new GoogleStrategy(
//     {
//       clientID: process.env.GOOGLE_CLIENT_ID as string,
//       clientSecret: process.env.GOOGLE_CLIENT_SECRET as string,
//       callbackURL: "http://localhost:8000/auth/google/redirect"
//     },
//     handleProfileCreation
//   )
// );

// passport.use(new FacebookStrategy({
//   clientID: process.env.FACEBOOK_APP_ID as string,
//   clientSecret: process.env.FACEBOOK_APP_SECRET as string,
//   callbackURL: "http://localhost:8000/auth/facebook/redirect",
//   display: 'popup',
//   profileFields: ["email", "displayName"]
// },
//   handleProfileCreation
// ));

// const app = express();

// app.use(cors());

// app.use(morgan('dev'));

// app.use(cookieParser());

// app.engine('html', mustacheExpress());
// app.set('view engine', 'mustache');
// app.set('views', __dirname + '/public');

// app.use(passport.initialize());

// app.use(express.static('src/public'));

// const getMe = async (req: express.Request) => {
//   let token: string = req.cookies.tkToken;
//   if(!token) {
//     token = req.headers['x-token'] as string;
//   }
//   const secret = process.env.SECRET as Secret;
//   if (token) {
//     try {
//       return await jwt.verify(token, secret);
//     } catch (e) {
//       throw new AuthenticationError(
//         'Your session expired. Sign in again.',
//       );
//     }
//   } else {
//     throw new AuthenticationError('Invalid session. Sign in again.');
//   }
// };

// const generateUserToken = (req: express.Request, res: express.Response) => {
//   const { id, emails } = cast(req.user);
//   const expiresIn = process.env.MODE as string === 'PRODUCTION'? '1 hour': '365 days';
//   const secret = process.env.SECRET as Secret;
//   const token = jwt.sign({ id, emails }, secret, {
//     expiresIn: expiresIn
//   });
//   res.cookie('tkToken', token, { httpOnly: true });
//   res.redirect('/?auth=success');
// }

// app.get("/auth/google", passport.authenticate('google', { display: 'popup', session: false, scope: ["profile", "email"] }));

// app.get("/auth/google/redirect", passport.authenticate('google', { session: false }), generateUserToken);

// app.get("/auth/facebook", passport.authenticate('facebook', { session: false }));

// app.get("/auth/facebook/redirect", passport.authenticate('facebook', { session: false }), generateUserToken);

// const server = new ApolloServer({
//   introspection: true,
//   typeDefs: schema,
//   resolvers,
//   formatError: error => {
//     // remove the internal sequelize error message
//     // leave only the important validation error
//     const message = error.message
//       .replace('SequelizeValidationError: ', '')
//       .replace('Validation error: ', '')

//     return {
//       ...error,
//       message,
//     };
//   },
//   context: async ({ req, connection }) => {
//     if (connection) {
//       return {
//         models,
//         loaders: {
//           user: new DataLoader((keys: string[]) =>
//             loaders.user.batchUsers(keys, models),
//           ),
//         },
//       };
//     }

//     if (req) {
//       const me = await getMe(req);
//       return {
//         models,
//         me,
//         secret: process.env.SECRET,
//         loaders: {
//           user: new DataLoader((keys: string[]) =>
//             loaders.user.batchUsers(keys, models),
//           ),
//         },
//       };
//     }
//   },
// });

// server.applyMiddleware({ app, path: '/graphql' });

// const httpServer = http.createServer(app);
// server.installSubscriptionHandlers(httpServer);

// const isTest = !!process.env.TEST_DATABASE_URL;
// const isProduction = process.env.NODE_ENV === 'production';
// const port = process.env.PORT || 8000;

// connectDb().then(async () => {
//   const UserModel: IUserModel = cast(models.User);
//   httpServer.listen({ port }, () => {
//     console.log(`Apollo Server on http://localhost:${port}/graphql`);
//   });
// });
