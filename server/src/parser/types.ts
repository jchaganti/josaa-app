export interface EntryObject {
  'institute': string,
  'type': number,
  'program': string,
  'quota': string,
  'seatType': string,
  'gender': string,
  'or': number,
  'cr': number
  'year': number,
  'round': number
}
