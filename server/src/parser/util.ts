
import * as _ from 'lodash';
import { INSTITUTE_TYPE, UNIQUE_INSTITUTES } from './data';
import { EntryObject } from './types';

export const getFieldValue = (elem: cheerio.Cheerio): string => {
  const span = elem.find('span');
  const text = span.length === 0 ? elem.text() : span.text();
  return text.trim().toUpperCase();
}

const getInstitute = (value: string): string => {
  value = value
    .replace('  ', ' ')
    .replace('-\n', '-')
    .replace('\n', '');
  return value;
}

const getFullProgram = (program: string): string[] => {
  let fourYrIndx = program.toLowerCase().indexOf('(4 year');
  if(fourYrIndx === -1) {
    fourYrIndx = program.toLowerCase().indexOf('(4year');
  }
  if(fourYrIndx > -1) {
    return [program.substring(0, fourYrIndx).trim(), program.substring(fourYrIndx).trim()];
  } else {
    let fiveYrIndx = program.toLowerCase().indexOf('(5 year');
    if(fiveYrIndx === -1) {
      fiveYrIndx = program.toLowerCase().indexOf('(5year');
    }
    if(fiveYrIndx > -1) {
      return [program.substring(0, fiveYrIndx).trim(), program.substring(fiveYrIndx).trim()];
    }
    return [program, '']
  }
}

const getProgram = (value: string): string => {
  const [program, _] = getFullProgram(value);
  return program;
}

const getInstituteType = (institute: string): number => {
  const insti = UNIQUE_INSTITUTES[institute];
  return (insti && insti.type) || INSTITUTE_TYPE.GFTI.value;  
}

const getDegree = (value: string): string => {
  const [_, degree] = getFullProgram(value);
  return degree.replace('(', '').replace(')', '');
}

const getSeatType = (value: string): string => {
  const seatType = value
    .replace('OPEN', 'GENERAL')
    .replace(' ', '')
    .replace('(', '')
    .replace(')', '')
    .replace('-', '')
    .replace('GENEWS','EWS')
    .replace('GENEWSPWD','EWSPWD');
  return seatType;
}

const getGender = (gender: string): string => {
  gender = gender.replace('NA', 'GENDERNEUTRAL').replace('-', '');
  const femaleOnly = 'FEMALEONLY';
  return gender.includes(femaleOnly) ? femaleOnly : gender;
}

const getOpeningRank = (or: string): number => {
  if(or.endsWith('P')) {
    or = or.substring(0, or.length - 1);
    return _.toInteger(or) * -1; 
  }
  return _.toInteger(or);
}

const getClosingRank = (cr: string): number => {
  if(cr.endsWith('P')) {
    cr = cr.substring(0, cr.length - 1);
    return _.toInteger(cr) * -1; 
  }
  return _.toInteger(cr);
}

const getQuota = (value: string): string => {
  return value;
}

const fieldvalueMapper = {
  'institute': getInstitute,
  'program': getProgram,
  'quota': getQuota,
  'seatType': getSeatType,
  'gender': getGender,
  'or': getOpeningRank,
  'cr': getClosingRank
}

export const getEntryObject = (lines: string [], year: string, round: number): EntryObject => {
  const entryObject: EntryObject =  Object.keys(fieldvalueMapper).reduce((acc, field, indx) => {
    acc[field] = fieldvalueMapper[field](lines[indx]);
    if(field === 'program') {
      acc['degree'] = getDegree(lines[indx] as unknown as string);
    } else if(field === 'institute') {
      acc['type'] = getInstituteType(acc[field]);
    }
    return acc;
  }, {}) as unknown as EntryObject;
  entryObject.year = parseInt(year);
  entryObject.round = round;
  return entryObject;
}

export const cast = <T extends unknown>(o: unknown): T => {
  return o as T;
}