import * as fs from "fs";
import { promisify } from "util";

import * as cheerio from 'cheerio'
import * as _ from 'lodash';
import { getFieldValue, getEntryObject, cast } from './util';
import { UNIQUE_SEAT_TYPES } from './data';
import { EntryObject } from './types';
import { isEmpty } from 'lodash';
import { IEntryObject, IEntryObjectModel } from '../models/entryObject';
import models from '../models';
const LineByLineReader = require('line-by-line')

let TOTAL_RECORDS = 0;

 type DBModelsMap = {[key: string]: IEntryObjectModel}
const dbModelsMap : DBModelsMap = {};
dbModelsMap['GENERAL'] = cast(models.GeneralSeat);
dbModelsMap['GENERALPWD'] = cast(models.GeneralPwDSeat);
dbModelsMap['EWS'] = cast(models.EWSSeat);
dbModelsMap['EWSPWD'] = cast(models.EWSPwDSeat);
dbModelsMap['OBCNCL'] = cast(models.OBCNCLSeat);
dbModelsMap['OBCNCLPWD'] = cast(models.OBCNCLPwDSeat);
dbModelsMap['SC'] = cast(models.SCSeat);
dbModelsMap['SCPWD'] = cast(models.SCPwDSeat);
dbModelsMap['ST'] = cast(models.STSeat);
dbModelsMap['STPWD'] = cast(models.STPwDSeat);

const handleHtmlFile = async (filename: string, year: string, round: number): Promise<EntryObject[]> => {
  const readFile = promisify(fs.readFile);
  const content = await readFile(filename, { encoding: "UTF-8" }).catch(e => {
    console.log('Error during load', e);
    return '';
  });
  const $ = cheerio.load(content, { ignoreWhitespace: true });
  const cutOffTable = $('.border_table_at');
  const entries = cutOffTable.find('tr');
  const entryObjects = entries.slice(1).map((_, entry) => {
    const cols = $(entry).find('td');
    const lines = cols.map((_, elem) => getFieldValue($(elem)));
    return getEntryObject(lines as unknown as string[], year, round);
  });

  return entryObjects.toArray().filter(entryObject => !isEmpty((entryObject as unknown as EntryObject).institute)) as unknown as EntryObject[];
}

const handleTextFile = async (filename: string, year: string, round: number): Promise<EntryObject[]> => {
  const lr = new LineByLineReader(filename, { skipEmptyLines: true });
  let entryLines: string[] = [];
  const entryObjects = [];
  let allTextHandled = false;
  const handleNewEntry = (line: string) => {
    entryLines.push(line.toUpperCase().trim());
    if (entryLines.length === 8) {
      lr.pause();
      if (entryLines[0] !== _.toString(round)) {
        console.log('@@@ Something wrong', entryLines[0], entryLines[2], entryLines[3])
      }
      entryLines.shift();
      entryObjects.push(getEntryObject(entryLines, year, round));
      entryLines = [];
      lr.resume();
    }
  }
  const handleEndEntry = (line: string) => {
    if (entryLines.length === 8) {
      // process last set of lines
      entryLines.shift();
      entryObjects.push(getEntryObject(entryLines, year, round));
      entryLines = [];
    }
    allTextHandled = true;
    return entryObjects;
  }
  lr.on('line', handleNewEntry);
  // process last set of lines in the tenLines buffer (if any)
  lr.on('end', handleEndEntry);
  return new Promise((resolve, reject) => {
    let tid = setInterval(() => {
      if (allTextHandled) {
        clearInterval(tid);
        tid = null;
        resolve(entryObjects.filter(entryObject => !isEmpty(entryObject.institute)));
      }
    }, 50);
  });
}
const uniqueRequiredFields = ['program'];
// const uniqueRequiredFields = ['seatType', 'quota', 'gender', 'institute'];

const getUniqueValues = (entryObjects, uniqueRequiredFields: string[]) => {
  const uniqueValues = uniqueRequiredFields.reduce((acc, field) => {
    const uniqueByFields = _.uniqBy(entryObjects, field);
    const uniqueFields = uniqueByFields.map(entryObject => entryObject[field]).filter(value => !_.isEmpty(value));
    acc[field] = uniqueFields;
    return acc;
  }, {});
  return uniqueValues;
}

const findDifferenceInValuesWRT2019 = (entryObjects: EntryObject[], field, referenceUniqueValues, year, round) => {
  const uniqueRequiredFields = [field];
  const uniqueValues = getUniqueValues(entryObjects, uniqueRequiredFields);
  const diffUniqueValues = _.difference(Object.keys(referenceUniqueValues), uniqueValues[field]);
  console.log(`DiffUnique${field}`, diffUniqueValues);
  console.log(`**Unique${field.toUpperCase()}Values**`, uniqueValues[field], 'year', year, 'round', round);
}

const handleEntryObjects = (entryObjects: EntryObject[], year, round) => {
  // findDifferenceInValuesWRT2019(entryObjects,'seatType', UNIQUE_SEAT_TYPES, year, round);
  // const uniqueValues = getUniqueValues(entryObjects, uniqueRequiredFields);
  // console.log('uniquePrograms', JSON.stringify(uniqueValues), 'year', year, 'round', round);
  const filteredEntryObjects = entryObjects.reduce((acc: any, entryObject: EntryObject) => {
    const { seatType } = entryObject;
    let seatTypeCollection: EntryObject[] = acc[seatType];
    if (!seatTypeCollection) {
      seatTypeCollection = [];
      acc[seatType] = seatTypeCollection;
    }
    seatTypeCollection.push(entryObject);
    return acc;
  }, {});
  const totalCollectedCount = Object.keys(UNIQUE_SEAT_TYPES).reduce((acc, seatType) => {
    return filteredEntryObjects[seatType] ? acc + filteredEntryObjects[seatType].length : acc;
  }, 0);
  TOTAL_RECORDS += totalCollectedCount;
  if (totalCollectedCount !== entryObjects.length) {
    console.log('@@@ totalCollectedCount', totalCollectedCount, 'entryObjects count', entryObjects.length, year, round);
    findDifferenceInValuesWRT2019(entryObjects, 'seatType', UNIQUE_SEAT_TYPES, year, round);
  }
  console.log('Current total records', TOTAL_RECORDS, 'year', year, 'round', round);
  Object.keys(filteredEntryObjects).forEach(async seatType => {
    const EntryObjectModel: IEntryObjectModel = dbModelsMap[seatType];
    const entryObjects: IEntryObject [] = filteredEntryObjects[seatType];
    const savedEntries: IEntryObject [] = await EntryObjectModel.insertMany(entryObjects);
    console.log("isLength same: ", savedEntries.length === entryObjects.length, 'seatType', seatType, 'year', year, 'round', round, '# of seats', entryObjects.length);
  })
}

export const handleFile = async (filename: string, year: string, round: number) => {
  const extension = filename.substring(filename.lastIndexOf('.') + 1);

  switch (extension) {
    case 'html': {
      const entryObjects: EntryObject[] = await handleHtmlFile(filename, year, round).catch(e => {
        console.log('Error in handleHtmlFile', e);
        return null;
      });
      if (entryObjects) {
        handleEntryObjects(entryObjects, year, round);
      }

      break;
    }
    case 'txt': {
      const entryObjects: EntryObject[] = await handleTextFile(filename, year, round).catch(e => {
        console.log('Error in handleHtmlFile', e);
        return null;
      });;
      if (entryObjects) {
        handleEntryObjects(entryObjects, year, round);
      }
      break;
    }
    default: {
      console.log('Unsupported extension');
      break;
    }
  }
}


