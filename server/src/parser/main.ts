import * as _ from 'lodash';
import { handleFile } from './fileHandler';
import { JOSAA_DATA, prefix } from './data';
import 'dotenv/config';
import mongoose from 'mongoose';

import  {connectDb} from '../models';


function loadData() {
  Object.keys(JOSAA_DATA).forEach(extension => {
    JOSAA_DATA[extension].forEach((yrsData) => {
      Object.keys(yrsData).forEach(year => {
        const rounds = yrsData[year] + 1;
        _.range(1, rounds).forEach(round => {
          const fileName = `${prefix}\\${year}\\josaa${year}Round${round}.${extension}`;
          console.log('handling file', fileName);
          handleFile(fileName, year, round);
        });
      });
    });
  });
}
// connectDb().then(async () => {
//   await mongoose.connection.dropDatabase();
//   console.log('Successfully connected and dropped old records')
//   loadData();
// })






