﻿function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}
function CandLogin_Validator(salt) {
//    Page_ClientValidate();
//    if (Page_IsValid) {
//    }
//    else {

//        return false;
    //    }
    if (document.getElementById('ctl00_ContentPlaceHolder1_rollno1').value == "") {
        alert('Please enter JEE(Main) Application Number.');
        return (false);
    }
    if (!isNumeric(document.getElementById('ctl00_ContentPlaceHolder1_rollno1').value)) {
        alert('Please enter valid JEE(Main) Application Number.');
        return (false);
    }
   

    if (document.getElementById('ctl00_ContentPlaceHolder1_Password1').value == "") {
        alert('Please enter Password.');
        return (false);
    }
    if (document.getElementById('ctl00_ContentPlaceHolder1_TxtSecPin')!= null) {
        if (document.getElementById('ctl00_ContentPlaceHolder1_TxtSecPin').value == "") {
            alert('Please enter Security Pin.');
            return (false);
        } 
    }
    if (document.getElementById('ctl00_ContentPlaceHolder1_Password1').value != "") {
        document.getElementById('ctl00_ContentPlaceHolder1_Password1').value = SHA256(document.getElementById('ctl00_ContentPlaceHolder1_Password1').value);
        document.getElementById('ctl00_ContentPlaceHolder1_Password1').value = SHA256(document.getElementById('ctl00_ContentPlaceHolder1_Password1').value + salt);
        var len = document.getElementById('ctl00_ContentPlaceHolder1_Password1').value.length;
        if (len > 64 || len < 64) {
            alert('Operation can not be performed.Your browser is not supporting Password Encryption.');
            return (false);
        }
        return (true);
    }
}


function CandLogin_ValidatorResult(salt) {
    if (document.getElementById('ctl00_ContentPlaceHolder1_Password1').value == "") {
        alert('Please enter Password.');
        return (false);
    }
    
    if (document.getElementById('ctl00_ContentPlaceHolder1_Password1').value != "") {
        document.getElementById('ctl00_ContentPlaceHolder1_Password1').value = SHA256(document.getElementById('ctl00_ContentPlaceHolder1_Password1').value);
        document.getElementById('ctl00_ContentPlaceHolder1_Password1').value = SHA256(document.getElementById('ctl00_ContentPlaceHolder1_Password1').value + salt);
        var len = document.getElementById('ctl00_ContentPlaceHolder1_Password1').value.length;
        if (len > 64 || len < 64) {
            alert('Operation can not be performed.Your browser is not supporting Password Encryption.');
            return (false);
        }
        return (true);
    }
}



function RefreshBttn() {

    document.getElementById('ctl00_ContentPlaceHolder1_rollno1').value = ""
    document.getElementById('ctl00_ContentPlaceHolder1_Password1').value = ""


}

function rblSelectedValue(rbl) {
    var selectedvalue = $("#" + rbl.id + " input:radio:checked").val();

    if (selectedvalue == "AC") {
        alert("Attention:\nYou have choosen ACCEPT, now you have to move futher and make the payment ")
    }

    if (selectedvalue == "RJ") {
        alert("Attention:\nYou have choosen REJECT, this will make s you ineligible for the subsequent round of counseling ")
    }
}


function ChgDeclaration() {

    var ChkIAgree = document.getElementById('ctl00_ContentPlaceHolder1_chkagreement');
    //  var btnFinalSubmit = document.getElementById('ctl00_ContentPlaceHolder1_btnFinalSubmit');
    var btnFinalSubmit = document.getElementById('ctl00_ContentPlaceHolder1_SubmitButton');
    var btnpayfee = document.getElementById('ctl00_ContentPlaceHolder1_resetbutton');
    if (ChkIAgree.checked == true) {
        btnFinalSubmit.disabled = false;
        btnpayfee.disabled =false;
        return;
    }
    if (ChkIAgree.checked == false) {
        btnFinalSubmit.disabled = true;
        btnpayfee.disabled = true;
        return;
    }
}

function DisableFinalSubmit() {
    var btnFinalSubmit = document.getElementById('ctl00_ContentPlaceHolder1_SubmitButton');
    btnFinalSubmit.disabled = true;
    var btnpayfee = document.getElementById('ctl00_ContentPlaceHolder1_resetbutton'); 
    btnpayfee.disabled = true;

}



